function show_date( number = "" ) {
    if ( !number ) return;

    let date = moment(number);
    let getday = date.days();//get days of date chat tim
    let today = moment().days();//get time currently

    let relToday = today - getday;

    switch(relToday){
        case 1:
            mess_date = 'Yesterday ' + date.format( 'HH:mm' );
            // console.log('Yesterday!!');
            break;
        case 0:
            // console.log('Today');
            mess_date = 'Today ' + date.format( 'HH:mm' );
            break;
        default:
            mess_date = date.format('DD MMMM HH:mm');
            // console.log(date.format('DD MMMM HH:mm'));      
    }

    return mess_date;

}



// =========================================================================================================
// =========================================================================================================
//  start 
// =========================================================================================================
// =========================================================================================================

jQuery(function($){



// =====================================================================================================
// var
// =====================================================================================================

var api_link = jQuery('[name="api_url"]').val();

var api_link_img = api_link ? 'https://xm.iptp.dev/xm/api' : 'https://xm.iptp.net/xm/api';

var link_ajax = live_assistance.admin_ajax;

var ls_token = localStorage.getItem("token") || "";
var ls_userId = localStorage.getItem("userId") || "";
var ls_chatId = localStorage.getItem("chatId") || "";
var ls_guestId = localStorage.getItem("guestId") || "";

var current_offset = 0;

var func_Current;
var t_timeout;

var check_sync = false;

var la__captcha;

console.log( ls_token );
console.log( 'ls_UserId' + ls_userId );
console.log( 'ls_chatId:' + ls_chatId );


// =====================================================================================================
// button send chat
// =====================================================================================================



 jQuery(document).on('submit','.js-form-live-assistant',function(e){

    e.preventDefault();

    // return;

    var $this = jQuery(this);

     // console.log('add clcik');


     if (  $this.find('.la__captcha-input').val() != la__captcha ){
        $('.la__captcha_mess').html('Captcha error');
        return;
     }



    //alert('ok');

        jQuery.ajax({
                
          type: "post",
          dataType: "json",
            url: link_ajax,
          data: {
                action:'register',
                name : $this.find('[name="name"]').val(),
                email : $this.find('[name="email"]').val(),
                departmentId : $this.find('[name="departmentId"]').val(),
                message : $this.find('[name="message"]').val(),
                api_link: api_link,
          },
          context: this,
          beforeSend: function() {

             // jQuery('.live-assistant__notice_response').removeClass('p-hidden');
            
            //  p_ajax_search_show_loading( form.find('.js-ajax-search-submit') );

                 // form
                 jQuery('#la__chat').addClass('p-hidden');
                 jQuery('#la__start').removeClass('p-hidden');
               


                 // button
                 $this.find('[type="submit"]').attr('disabled',true);
                //  $this.find('[type="submit"]').find('i').show();
                
                 // remove
                 jQuery('.la__status').html('');

                 jQuery('.la__mess').html('');


                      jQuery('.js-new-chat').attr('disabled',false);
                     jQuery('.js-new-chat').find('i').hide();
                    
                       jQuery('.js-return-chat').attr('disabled',false);
                      // jQuery('.js-return-chat').find('i').hide();




          },

          success: function(response) {
                

                if ( response.token ) {



                    // var ar = [];
                    jQuery('#la__start').addClass('p-hidden'); 
                    jQuery('#la__chat').removeClass('p-hidden');                    


                 //   console.log( response );

                    localStorage.setItem("token",  response.token );
                    localStorage.setItem("userId",  response.userId );
                    localStorage.setItem("chatId",  response.chatId );
                    localStorage.setItem("guestId",  response.guestId );

                    ls_token = localStorage.getItem("token");
                    ls_userId = localStorage.getItem("userId");
                    ls_chatId = localStorage.getItem("chatId");
                    ls_guestId = localStorage.getItem("guestId");

                     jQuery('.btn-primary-show-mess').click();


                     setTimeout(function(){

                        live_load_chat();
                        t_timeout = sync_chat();

                     }, 2000 );
                    


                   


                } else {



                    if ( response.details && response.status ){

                     //   jQuery('#la__chat').addClass('p-hidden');
                        jQuery('#la__start').removeClass('p-hidden');


                        jQuery('.la__status').html( response.message ).show();



                        // $this.find('[type="submit"]').hide();

                    
                        jQuery('.js-return-chat').show();
                        jQuery('.js-new-chat').show();


                        jQuery('.live-click-logout-2').show();


                        jQuery('#la__warning').addClass('active');


                    }




                }

                // jQuery('.live-assistant__notice_response').removeClass('p-hidden');
                // jQuery('#la__start').addClass('p-hidden');
           

            //     // aftersend      
                $this.find('[type="submit"]').attr('disabled',false);
                // $this.find('[type="submit"]').find('i').hide();

          },

          error: function(jqXHR, textStatus, errorThrown) {
             // location.href = location.href;
              //p_user_loadding_after();

                $this.find('[type="submit"]').attr('disabled',false);
                // $this.find('[type="submit"]').find('i').hide();


          }


    });

});


// =====================================================================================================
// button restore
// =====================================================================================================

 jQuery(document).on('click','.js-return-chat',function(e){
    e.preventDefault();

    if ( !ls_guestId ) return;


    // return;

    var $this = jQuery(this);

     // console.log('add clcik');


    //alert('ok');

        jQuery.ajax({
                
          type: "post",
          dataType: "json",
            url: link_ajax,
          data: {
               action:'restore',
               guestId : ls_guestId,
               api_link: api_link,
          },
          context: this,
          beforeSend: function() {

          
                 // button
                 $this.attr('disabled',true);
                 $this.find('i').show();
                
                 jQuery('.js-new-chat').attr('disabled',true);
               //  $('.js-new-chat').find('i').show();

               jQuery('.la__mess').html('');
               jQuery('.la__status').html('');
          },

          success: function(response) {
                
              
                if ( response.token ) {


        
                    localStorage.setItem("token",  response.token );
                    localStorage.setItem("userId",  response.userId );
                    localStorage.setItem("chatId",  response.chatId );
                    localStorage.setItem("guestId",  response.guestId );


                    ls_token = localStorage.getItem("token");
                    ls_userId = localStorage.getItem("userId");
                    ls_chatId = localStorage.getItem("chatId");
                    ls_guestId = localStorage.getItem("guestId");


                    jQuery('.la__status').hide();
                    jQuery('.live-click-logout-2').hide();

                    jQuery('.js-return-chat').hide();
                    jQuery('.js-new-chat').hide();




                    setTimeout(function(){

                      live_load_chat(3000, 0, true );
                      t_timeout = sync_chat();

                    }, 2000);
                    




                    // live_load_chat(3000, 0, true );
                    // t_timeout = sync_chat();


                    setTimeout(function(){
                        $('.la__mess').scrollTop($('.la__mess')[0].scrollHeight);
                        
                    }, 2500 );
                  



                    jQuery('.btn-primary-show-mess').click();



                       $this.attr('disabled',false);
                       $this.find('i').hide();
                    
                       jQuery('.js-new-chat').attr('disabled',false);
                      // $('.js-new-chat').find('i').hide();



                } else {

                  
                    if ( response.details && response.status  ){

                     //   jQuery('#la__chat').addClass('p-hidden');
                      //  jQuery('#la__start').addClass('p-hidden');
                        console.log( 'response.details2' );
                         console.log( response.details );



                      jQuery('.la__status').html("<div style='color:red'>" + response.message + "</div>").show();


                    
                       $this.attr('disabled',false);
                       $this.find('i').hide();
                    
                       jQuery('.js-new-chat').attr('disabled',false);
                      // $('.js-new-chat').find('i').hide();

                    }


                }



          },

          error: function(jqXHR, textStatus, errorThrown) {
             // location.href = location.href;
              //p_user_loadding_after();

              
                 // button
                 // $this.find('[type="submit"]').attr('disabled',true);
                 // $this.find('[type="submit"]').find('i').show();
                
                 // $('.js-new-chat').attr('disabled',true);
                 // $('.js-new-chat').find('[type="submit"]').find('i').show();



          }


    });

});



// =====================================================================================================
// button new chat
// =====================================================================================================

 jQuery(document).on('click','.js-new-chat',function(e){

    e.preventDefault();

    // return;

    var $this_form = jQuery('.js-form-live-assistant');

     // console.log('add clcik');


    //alert('ok');

        jQuery.ajax({
                
          type: "post",
          dataType: "json",
            url: link_ajax,
          data: {


              action:'newchat',
              name : $this_form.find('[name="name"]').val(),
              email : $this_form.find('[name="email"]').val(),
              departmentId : $this_form.find('[name="departmentId"]').val(),
              message : $this_form.find('[name="message"]').val(),
               api_link: api_link,
              
          },
          context: this,
          beforeSend: function() {

          
                 // button
                 $this_form.attr('disabled',true);
                $this_form.find('i').show();
                
                 jQuery('.js-return-chat').attr('disabled',true);
               //  $('.js-new-chat').find('i').show();

               jQuery('.la__mess').html('');

               jQuery('.la__status').html('');
          },

          success: function(response) {
                
              
                if ( response.token ) {

                    localStorage.setItem("token",  response.token );
                    localStorage.setItem("userId",  response.userId );
                    localStorage.setItem("chatId",  response.chatId );
                    localStorage.setItem("guestId",  response.guestId );

                    ls_token = localStorage.getItem("token");
                    ls_userId = localStorage.getItem("userId");
                    ls_chatId = localStorage.getItem("chatId");
                    ls_guestId = localStorage.getItem("guestId");


                    jQuery('.la__status').hide();
                    jQuery('.live-click-logout-2').hide();

                    jQuery('.js-return-chat').hide();
                    jQuery('.js-new-chat').hide();

                    live_load_chat(3000, 0, true );
                    t_timeout = sync_chat();
                    //t_timeout = sync_chat();

                    jQuery('.btn-primary-show-mess').click();



                       $this_form.attr('disabled',false);
                      // $this_form.find('i').hide();
                    
                       jQuery('.js-return-chat').attr('disabled',false);
                      // $('.js-new-chat').find('i').hide();



                } else {

                  
                    if ( response.details && response.status ){

                     //   jQuery('#la__chat').addClass('p-hidden');
                      //  jQuery('#la__start').addClass('p-hidden');
                        console.log( 'response.details2' );
                         console.log( response.details );



                      jQuery('.la__status').html("<div style='color:red'>" + response.message + "</div>").show();


                    
                       $this_form.attr('disabled',false);
                       $this_form.find('i').hide();
                    
                       jQuery('.js-new-chat').attr('disabled',false);
                      // $('.js-new-chat').find('i').hide();

                    }

                }

          },

          error: function(jqXHR, textStatus, errorThrown) {
             // location.href = location.href;
              //p_user_loadding_after();

              
                 // button
                 // $this.find('[type="submit"]').attr('disabled',true);
                 // $this.find('[type="submit"]').find('i').show();
                
                 // $('.js-new-chat').attr('disabled',true);
                 // $('.js-new-chat').find('[type="submit"]').find('i').show();
          }


    });

});


// =====================================================================================================
// live load chat
// =====================================================================================================
    function live_load_chat( time = 3000, offset = 0, sync = false ){
     //   console.log('start_ive_realtime_chat');

        
        if ( ls_token && ls_userId && ls_chatId ) {

         //   console.log('start_ive_realtime_chat_2');

            jQuery('#la__chat').removeClass('p-hidden');
            jQuery('#la__start').addClass('p-hidden');  


            var settings = {
                
                type: "post",
                dataType: "json",
                url: link_ajax,
                data: {
                    action: 'load_mess',
                  //    abc:'test',
                    token: ls_token,
                    userId: ls_userId,
                    chatId: ls_chatId,
                    offset:offset,
                     api_link: api_link,
                },

            };

         // clearTimeout( t_timeout );
         // t_timeout = load_mess( settings, 0 );


            $.ajax(settings).done(function (response) {
                //jQuery('.la__mess').html('');


                // message: "Chat not found or deleted!"
                if (  response.status == "2" ){

                    var template2 = jQuery('#template2').html();

                    var mess_html2 = template2;


                        mess_html2 = mess_html2.replace("{{message}}", response.message );


                    jQuery('.la__mess').prepend(  mess_html2 );

                  return;
                }

               //console.log('response');
              // console.log(response);

                var ob_mess = response.data.messages;

                // console.log('ob_mess');
               //console.log(ob_mess);



                for( var i in ob_mess ) {


                            var load_mess_html2 = load_mess_html( ob_mess[i] ); 

                        if ( ob_mess[i].updated == true ){

                          if (  jQuery(document).find('div.messages__time-chat[data-mess-id="'+ob_mess[i].id.messageId+'"]').length  ) {

                             jQuery(document).find('div.messages__time-chat[data-mess-id="'+ob_mess[i].id.messageId+'"]').find('.--msg').html(  ob_mess[i].message   );


                           } else {

                              jQuery(document).find('.la__mess').prepend( load_mess_html2   );



                          }


                        } else {

                          if ( jQuery(document).find('div.messages__time-chat[data-mess-id="'+ob_mess[i].id.messageId+'"]').length <= 0 ) {

                              jQuery(document).find('.la__mess').prepend( load_mess_html2   );
                            
                          }


                        }
                 
                }

                // clearTimeout( t_timeout );

                //  t_timeout = timeout( settings, 3000  );

                // if ( !check_sync || sync == true ){
                //     check_sync = true;

                //    // sync_chat();


                //   //  clearTimeout( t_timeout );

                //      jQuery('.--msg').readmore({ 

                //       collapsedHeight:127,
                //       speed: 75, 
                //       blockCSS: 'display:block; width: 100%;',
                //       moreLink: '<a href="#" class="btn__readmore">Read more <i class="fa fa-chevron-down" aria-hidden="true" style="margin-left:5px"></i></a>',
                //       lessLink: '<a href="#" class="btn__readmore">Close <i class="fa fa-chevron-up" aria-hidden="true" style="margin-left:5px"></i></a>',

                //     });




                //      t_timeout = sync_chat();


                 
                     
                  


                // }


            });
   
      }
        

    } // func


    function load_mess_html( mess_re = "" ){
        if ( !mess_re ) return;




        var template = jQuery('#template1').html();

        var mess_html = template;


        mess_html = mess_html.replace("{{class_position}}", mess_re.sender.id == ls_userId ? "--owner" : "" );

        mess_html = mess_html.replace("{{name}}", mess_re.sender.name );

        mess_html = mess_html.replace("{{mess_id}}", mess_re.id.messageId );



         // var hour = moment( mess_re.sender.lastActivityAt ).format('HH:mm');
         // var date = moment( mess_re.sender.lastActivityAt ).format('MM/DD/yyyy');
         // var date_all = moment( mess_re.sender.lastActivityAt ).format('DD MMM HH:mm');
       //  console.log( 'time: ' + mess_re.sender.lastActivityAt   );



          mess_html = mess_html.replace("{{date}}",  show_date( mess_re.msgDate ) );
        // mess_html = mess_html.replace("{{date}}", date_all  );


        var mess_send = mess_re.message;

        // if ( mess_re.file.contentType.indexOf( "image/" ) === 0 ) {

        //     mess_send = "<img src='" + api_link + '/image?id=' + mess_re.file.id + '&large=1' + "' alt='img'>";


        // }




        var avatar = "";

        if ( mess_re.sender.id != ls_userId ){
          avatar = '<img src="' + api_link_img + '/getavatar?id=' + mess_re.sender.id + '" alt="img" style="border-radius:50%;width:25px;margin-right:8px;margin-bottom:5px">';
        }

        mess_html = mess_html.replace("{{avatar}}", avatar );







         mess_html =  mess_html.replace("{{message}}", mess_send );


        return mess_html;

    }







// =====================================================================================================
// sync chat
// =====================================================================================================
 function sync_chat( time = 10000 ){

    

        if ( ls_token && ls_userId && ls_chatId ) {

            var settings = {
                
                type: "post",
                  dataType: "json",
                  url: link_ajax,
                  data: {
                    action: 'sync_mess',
                  //    abc:'test',
                    token: ls_token,
                    userId: ls_userId,
                    chatId: ls_chatId,
                    timeout:10000,
                    api_link: api_link,
                  },

            };

         // clearTimeout( t_timeout );
         // t_timeout = load_mess( settings, 0 );

            $.ajax(settings).done(function (response) {
              
               if ( response  ) {

                if ( response.data != null ) {

                  if ( response.data.messages != null ) {

                    // console.log(response);
                      var ob_mess = response.data.messages;

                     // console.log( 'ob_mess' );
                     // console.log( ob_mess );


                      for( var i in ob_mess ) {

                        var load_mess_html2 = load_mess_html( ob_mess[i] ); 

                        if ( ob_mess[i].updated == true ){

                          if (  jQuery(document).find('div.messages__time-chat[data-mess-id="'+ob_mess[i].id.messageId+'"]').length  ) {


                          
                            jQuery(document).find('div.messages__time-chat[data-mess-id="'+ob_mess[i].id.messageId+'"]').find('.--msg').html(  ob_mess[i].message   );


                           } else {

                              jQuery(document).find('.la__mess').append( load_mess_html2   );



                          }


                        } else {

                          if ( jQuery(document).find('div.messages__time-chat[data-mess-id="'+ob_mess[i].id.messageId+'"]').length <= 0 ) {

                              jQuery(document).find('.la__mess').append( load_mess_html2   );
                            
                          }

                        }


                      }

                       jQuery('.--msg').readmore({ 

                            collapsedHeight:127,
                            speed: 75, 
                            blockCSS: 'display:block; width: 100%;',
                            moreLink: '<a href="#" class="btn__readmore">Read more <i class="fa fa-chevron-down" aria-hidden="true" style="margin-left:5px"></i></a>',
                            lessLink: '<a href="#" class="btn__readmore">Close <i class="fa fa-chevron-up" aria-hidden="true" style="margin-left:5px"></i></a>',

                      });

                 }

               }

                   // sync_chat();

                t_timeout = sync_chat();


              } else {

                  console.log('An error occurred during synchronization. Will try again in 5 seconds...');

                  
                  setTimeout(function(){ t_timeout = sync_chat(); }, 5000);

              }

              

                 //  clearTimeout( t_timeout );

                   // setTimeout(function(){
                   //    sync_chat();
                   // }, time );
              
            });

           
      }

 

    
} // func






// =====================================================================================================
// button send chat
// =====================================================================================================


  jQuery(".la__chat--input").keypress(function (e) {
      var code = (e.keyCode ? e.keyCode : e.which);
      //alert(code);
      if (code == 13 && !e.shiftKey ) {
          jQuery(".live-assistant__btn-send-chat,.la__chat--send").trigger('click');
          return false;
      }
  });




jQuery(document).on('click',  '.live-assistant__btn-send-chat,.la__chat--send' , function(e) {

        e.preventDefault();

        if ( jQuery('.la__chat--input').val() == "" ){
             jQuery('.la__chat--input').focus();

             return;
        }

        
        var $this = $(this);


        jQuery.ajax({
            
      type: "post",
      dataType: "json",
      url: link_ajax,
      data: {
            
            action: 'send_mess',
              //    abc:'test',
            token: ls_token,
            userId: ls_userId,
            chatId: ls_chatId,
            mess: $('.la__chat--input').val(),
             api_link: api_link,
      },
      context: this,
      beforeSend: function() {

         // jQuery('.live-assistant__notice_response').removeClass('p-hidden');
        
        //  p_ajax_search_show_loading( form.find('.js-ajax-search-submit') );

             // form
          //    jQuery('#la__chat').addClass('p-hidden');
             // jQuery('#la__start').removeClass('p-hidden');


             // // button
             $this.attr('disabled',true);
            //  $this.find('i').show();
            
            //  // remove
          //    jQuery('.la__status').html('');

           jQuery('.la__chat--input').val('');

          // live_load_chat();


          t_timeout = sync_chat();

      },

      success: function(response) {

         $this.attr('disabled', false );
          // $this.find('i').hide();

       //  func_Current = live_load_chat( 1 );

       
        // clearTimeout( t_timeout );

        

          // t_timeout = sync_chat();


           
           jQuery('.--msg').readmore({ 

            collapsedHeight:127,
            speed: 75, 
            blockCSS: 'display:block; width: 100%;',
            moreLink: '<a href="#" class="btn__readmore">Read more <i class="fa fa-chevron-down" aria-hidden="true" style="margin-left:5px"></i></a>',
            lessLink: '<a href="#" class="btn__readmore">Close <i class="fa fa-chevron-up" aria-hidden="true" style="margin-left:5px"></i></a>',

          });

          
    
    
      },

      error: function(jqXHR, textStatus, errorThrown) {
         // location.href = location.href;
          //p_user_loadding_after();

            // $this.find('[type="submit"]').attr('disabled',false);
            // $this.find('[type="submit"]').find('i').hide();


      }


    });





});

//js toggle div data-target
$(".js-click").click(function(){

  var target = $(this).attr("data-click");

  if($(target).hasClass('active')){
    $(target).removeClass('active');

    $('.js-form-live-assistant')[0].reset();

    $('.la__captcha-img_change').trigger('click');

  }
  else{
    $(target).addClass('active');
  }

});


// //close popup
$('.js-close').click(function(){
  $('[data-target2]').trigger( "click" );
});



$('[data-target2]').click(function(){

  if ( !$('.la__inner').hasClass('active') ) {

      $('.la__inner').addClass('active');

  } else {

      $('.la__inner').removeClass('active');

  }


});







// =========================================================================================================
//  clear session
// =========================================================================================================
jQuery('.live-click-logout,.live-click-logout-2').on('click',function(e){
    e.preventDefault();
    location.href = location.href + '?remove';


})



// =========================================================================================================
// load offset
// =========================================================================================================

jQuery('.la__mess').on('scroll',function() {

    if ( jQuery(this).scrollTop() > 100 ) return;

  //  console.log( $(this).scrollTop() );


    if ( current_offset > 1  ) {
        
        live_load_chat('', current_offset );

    }
      
      
});





// =========================================================================================================
// click button logout
// =========================================================================================================
jQuery(document).on('click','.js-logout', function(){
    
    if (  $('#la__closing').hasClass('active') ){

       $('#la__closing').addClass('active');

    } else {
      
      $('#la__closing').removeClass('active');

    }

});

jQuery(document).on('click','.js-no-logout', function(){
     $('#la__closing').removeClass('active'); 
});




jQuery(document).on('click','.js-yes-logout', function(){

    localStorage.removeItem("token");
    localStorage.removeItem("userId");
    localStorage.removeItem("chatId");

    ls_token = "";
    ls_userId = "";
    ls_chatId = "";


   jQuery('#la__chat').addClass('p-hidden');
   jQuery('#la__start').removeClass('p-hidden');  


   jQuery('.la__captcha-img_change').trigger('click');

    clearTimeout( t_timeout );

    jQuery('.js-form-live-assistant')[0].reset();


     jQuery('#la__closing').removeClass('active');

      jQuery('#la__warning').removeClass('active');

});













// =========================================================================================================
// click button logout
// =========================================================================================================

 function la__captcha_func(){
     var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z', 
            '0','1','2','3','4','5','6','7','8','9');
     var i;
    // for (i=0;i<5;i++){
       var a = alpha[Math.floor(Math.random() * alpha.length)];
       var b = alpha[Math.floor(Math.random() * alpha.length)];
       var c = alpha[Math.floor(Math.random() * alpha.length)];
       var d = alpha[Math.floor(Math.random() * alpha.length)];
       var e = alpha[Math.floor(Math.random() * alpha.length)];
      // var f = alpha[Math.floor(Math.random() * alpha.length)];
       //var g = alpha[Math.floor(Math.random() * alpha.length)];
   // }

    return la__captcha = a  + b  + c  + d + e ;
         
}

// la__captcha();







if ( $('.la__captcha-img_change').length ){
  var rotate1 = 360;
  $(document).on('click', '.la__captcha-img_change', function(){
      $(this).css('transform', 'rotate(' + rotate1 + 'deg)');

        rotate1 += 360;

       $('.la__captcha_mess').html('');
       $('.la__captcha-img').html( la__captcha_func() );

       $('.la__captcha-input').val('');

  });


   $('.la__captcha-img_change').trigger('click');
}






if ( ls_token && ls_userId && ls_chatId ) {
    live_load_chat();
    t_timeout = sync_chat();

      setTimeout(function(){
          $('.la__mess').scrollTop($('.la__mess')[0].scrollHeight);
      },100);
    


} else {

    jQuery('#la__chat').addClass('p-hidden');
    jQuery('#la__start').removeClass('p-hidden');  

}






});

