<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Conversation Example</title>
    <style>
        #messages {
            margin-bottom: 10px;
            overflow-y: scroll;
            height: 200px;
            border: 1px solid #ccc;
            padding: 10px;
        }
    </style>
</head>
<body>
    <div id="conversation">
        <h1>Chat with IPTP</h1>
        <div id="messages"></div>
        <input type="text" id="inputMessage" placeholder="Type a message...">
        <button id="sendMessage">Send</button>
    </div>

    <script>
        // Initialize variables
        var messageContainer = document.getElementById("messages");
        var messageInput = document.getElementById("inputMessage");
        var sendMessageButton = document.getElementById("sendMessage");

        // Add event listener to the send button
        sendMessageButton.addEventListener("click", sendMessage);

        // Add event listener to the input field
        messageInput.addEventListener("keypress", function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                sendMessage();
            }
        });

        // Function to send a message
        function sendMessage() {
            var messageText = messageInput.value;

            // Clear the input field
            messageInput.value = "";

            // Add the user's message to the message container
            var messageElement = document.createElement("div");
            messageElement.classList.add("user-message");
            messageElement.innerText = "You: " + messageText;
            messageContainer.appendChild(messageElement);

            // Call the bot to respond
            getBotResponse(messageText);
        }

        // Function to get a response from the bot
        function getBotResponse(message) {
            // Send the user's message to the server
            // using AJAX or fetch
            // In this example, we will just use a dummy response

            var botResponse = "I'm sorry, I don't understand.";

            // Add the bot's message to the message container
            var messageElement = document.createElement("div");
            messageElement.classList.add("bot-message");
            messageElement.innerText = "ChatGPT: " + botResponse;
            messageContainer.appendChild(messageElement);
        }
    </script>
</body>
</html>
