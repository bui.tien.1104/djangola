function show_date( number = "" ) {
    if ( !number ) return;

    let date = moment(number);
    let getday = date.days();//get days of date chat tim
    let today = moment().days();//get time currently

    let relToday = today - getday;

    switch(relToday){
        case 1:
            mess_date = 'Yesterday ' + date.format( 'HH:mm' );
            // console.log('Yesterday!!');
            break;
        case 0:
            // console.log('Today');
            mess_date = 'Today ' + date.format( 'HH:mm' );
            break;
        default:
            mess_date = date.format('DD MMMM HH:mm');
            // console.log(date.format('DD MMMM HH:mm'));      
    }

    return mess_date;

}



// =========================================================================================================
// =========================================================================================================
//  start 
// =========================================================================================================
// =========================================================================================================

jQuery(function($){



// =====================================================================================================
// var
// =====================================================================================================

var link_ajax = '/test-live/php/index.php';

var ls_token = localStorage.getItem("token") || "";
var ls_userId = localStorage.getItem("userId") || "";
var ls_chatId = localStorage.getItem("chatId") || "";

var current_offset = 0;

var func_Current;
var t_timeout;

var check_sync = false;


// console.log( ls_token );
// console.log( 'ls_UserId' + ls_userId );
// console.log( 'ls_chatId:' + ls_chatId );


// =====================================================================================================
// button send chat
// =====================================================================================================



 jQuery(document).on('submit','.js-form-live-assistant',function(e){

    e.preventDefault();

    // return;

    var $this = jQuery(this);

     // console.log('add clcik');


    //alert('ok');

        jQuery.ajax({
                
          type: "post",
          dataType: "json",
            url: link_ajax,
          data: {
                action:'register',
                name : $this.find('[name="name"]').val(),
                email : $this.find('[name="email"]').val(),
                departmentId : $this.find('[name="departmentId"]').val(),
                message : $this.find('[name="message"]').val(),

          },
          context: this,
          beforeSend: function() {

             // jQuery('.live-assistant__notice_response').removeClass('p-hidden');
            
            //  p_ajax_search_show_loading( form.find('.js-ajax-search-submit') );

                 // form
                 jQuery('.live-assistant__wrap_notice_mess').addClass('p-hidden');
                 jQuery('.live-assistant__wrap_form').removeClass('p-hidden');


                 // button
                 $this.find('[type="submit"]').attr('disabled',true);
                 $this.find('[type="submit"]').find('i').show();
                
                 // remove
                 jQuery('.live-assistant__form_status').html('');
          },

          success: function(response) {
                

                if ( response.token ) {

                    // var ar = [];

                    jQuery('.live-assistant__wrap_notice_mess').removeClass('p-hidden');
                    jQuery('.live-assistant__wrap_form').addClass('p-hidden');  

                    
                 //   console.log( response );

                    localStorage.setItem("token",  response.token );
                    localStorage.setItem("userId",  response.userId );
                    localStorage.setItem("chatId",  response.chatId );


                    ls_token = localStorage.getItem("token");
                    ls_userId = localStorage.getItem("userId");
                    ls_chatId = localStorage.getItem("chatId");






                    live_load_chat();

                    jQuery('.btn-primary-show-mess').click();




                } else {

                    if ( response.details && response.status == 400 ){

                     //   jQuery('.live-assistant__wrap_notice_mess').addClass('p-hidden');
                        jQuery('.live-assistant__wrap_form').addClass('p-hidden');


                        jQuery('.live-assistant__form_status').html( "<div style='color:red'>" + response.details + "</div>" );



                        $this.find('[type="submit"]').hide();

                    
                        jQuery('.js-live-btn-return-chat').show();
                        jQuery('.js-live-btn-new-chat').show();


                        jQuery('.live-click-logout-2').show();


                    }




                }



                // jQuery('.live-assistant__notice_response').removeClass('p-hidden');
                // jQuery('.live-assistant__wrap_form').addClass('p-hidden');
           

            //     // aftersend      
                $this.find('[type="submit"]').attr('disabled',false);
                $this.find('[type="submit"]').find('i').hide();


             



          },

          error: function(jqXHR, textStatus, errorThrown) {
             // location.href = location.href;
              //p_user_loadding_after();

                $this.find('[type="submit"]').attr('disabled',false);
                $this.find('[type="submit"]').find('i').hide();


          }


    });

});








// =====================================================================================================
// button restore
// =====================================================================================================

 jQuery(document).on('click','.js-live-btn-return-chat',function(e){

    e.preventDefault();

    // return;

    var $this = jQuery(this);

     // console.log('add clcik');


    //alert('ok');

        jQuery.ajax({
                
          type: "post",
          dataType: "json",
            url: link_ajax,
          data: {
              action:'restore',
              email : jQuery('.js-form-live-assistant').find('[name="email"]').val(),
              
          },
          context: this,
          beforeSend: function() {

          
                 // button
                 $this.attr('disabled',true);
                 $this.find('i').show();
                
                 jQuery('.js-live-btn-new-chat').attr('disabled',true);
               //  $('.js-live-btn-new-chat').find('i').show();


          },

          success: function(response) {
                
              
                if ( response.token ) {


                    localStorage.setItem("token",  response.token );
                    localStorage.setItem("userId",  response.userId );
                    localStorage.setItem("chatId",  response.chatId );


                    ls_token = localStorage.getItem("token");
                    ls_userId = localStorage.getItem("userId");
                    ls_chatId = localStorage.getItem("chatId");


                    jQuery('.live-assistant__form_status').hide();
                    jQuery('.live-click-logout-2').hide();

                    jQuery('.js-live-btn-return-chat').hide();
                    jQuery('.js-live-btn-new-chat').hide();





                    live_load_chat();

                    jQuery('.btn-primary-show-mess').click();



                       $this.attr('disabled',false);
                       $this.find('i').hide();
                    
                       jQuery('.js-live-btn-new-chat').attr('disabled',false);
                      // $('.js-live-btn-new-chat').find('i').hide();



                } else {

                  
                    if ( response.details && response.status == 404 ){

                     //   jQuery('.live-assistant__wrap_notice_mess').addClass('p-hidden');
                      //  jQuery('.live-assistant__wrap_form').addClass('p-hidden');
                        console.log( 'response.details2' );
                         console.log( response.details );



                      jQuery('.live-assistant__form_status').html("<div style='color:red'>" + response.details + "</div>");


                    
                       $this.attr('disabled',false);
                       $this.find('i').hide();
                    
                       jQuery('.js-live-btn-new-chat').attr('disabled',false);
                      // $('.js-live-btn-new-chat').find('i').hide();

                    }
                 






                }



          },

          error: function(jqXHR, textStatus, errorThrown) {
             // location.href = location.href;
              //p_user_loadding_after();

              
                 // button
                 // $this.find('[type="submit"]').attr('disabled',true);
                 // $this.find('[type="submit"]').find('i').show();
                
                 // $('.js-live-btn-new-chat').attr('disabled',true);
                 // $('.js-live-btn-new-chat').find('[type="submit"]').find('i').show();



          }


    });

});









// =====================================================================================================
// button new chat
// =====================================================================================================

 jQuery(document).on('click','.js-live-btn-new-chat',function(e){

    e.preventDefault();

    // return;

    var $this_form = jQuery('.js-form-live-assistant');

     // console.log('add clcik');


    //alert('ok');

        jQuery.ajax({
                
          type: "post",
          dataType: "json",
            url: link_ajax,
          data: {


              action:'newchat',
              name : $this_form.find('[name="name"]').val(),
              email : $this_form.find('[name="email"]').val(),
              departmentId : $this_form.find('[name="departmentId"]').val(),
              message : $this_form.find('[name="message"]').val(),

              
          },
          context: this,
          beforeSend: function() {

          
                 // button
                 $this_form.attr('disabled',true);
                $this_form.find('i').show();
                
                 jQuery('.js-live-btn-return-chat').attr('disabled',true);
               //  $('.js-live-btn-new-chat').find('i').show();


          },

          success: function(response) {
                
              
                if ( response.token ) {


                    localStorage.setItem("token",  response.token );
                    localStorage.setItem("userId",  response.userId );
                    localStorage.setItem("chatId",  response.chatId );


                    ls_token = localStorage.getItem("token");
                    ls_userId = localStorage.getItem("userId");
                    ls_chatId = localStorage.getItem("chatId");


                    jQuery('.live-assistant__form_status').hide();
                    jQuery('.live-click-logout-2').hide();

                    jQuery('.js-live-btn-return-chat').hide();
                    jQuery('.js-live-btn-new-chat').hide();





                    live_load_chat();

                    jQuery('.btn-primary-show-mess').click();



                       $this_form.attr('disabled',false);
                      $this_form.find('i').hide();
                    
                       jQuery('.js-live-btn-return-chat').attr('disabled',false);
                      // $('.js-live-btn-new-chat').find('i').hide();



                } else {

                  
                    if ( response.details && response.status == 404 ){

                     //   jQuery('.live-assistant__wrap_notice_mess').addClass('p-hidden');
                      //  jQuery('.live-assistant__wrap_form').addClass('p-hidden');
                        console.log( 'response.details2' );
                         console.log( response.details );



                      jQuery('.live-assistant__form_status').html("<div style='color:red'>" + response.details + "</div>");


                    
                       $this_form.attr('disabled',false);
                       $this_form.find('i').hide();
                    
                       jQuery('.js-live-btn-new-chat').attr('disabled',false);
                      // $('.js-live-btn-new-chat').find('i').hide();

                    }
                 






                }



          },

          error: function(jqXHR, textStatus, errorThrown) {
             // location.href = location.href;
              //p_user_loadding_after();

              
                 // button
                 // $this.find('[type="submit"]').attr('disabled',true);
                 // $this.find('[type="submit"]').find('i').show();
                
                 // $('.js-live-btn-new-chat').attr('disabled',true);
                 // $('.js-live-btn-new-chat').find('[type="submit"]').find('i').show();



          }


    });

});





















// =====================================================================================================
// live load chat
// =====================================================================================================
    function live_load_chat( time = 3000, offset = 0 ){
     //   console.log('start_ive_realtime_chat');

        
        if ( ls_token && ls_userId && ls_chatId ) {

         //   console.log('start_ive_realtime_chat_2');

            jQuery('.live-assistant__wrap_notice_mess').removeClass('p-hidden');
            jQuery('.live-assistant__wrap_form').addClass('p-hidden');  


            var settings = {
                
                type: "post",
                dataType: "json",
                url: link_ajax,
                data: {
                    action: 'load_mess',
                  //    abc:'test',
                    token: ls_token,
                    userId: ls_userId,
                    chatId: ls_chatId,
                    offset:offset,
                    
                },

            };

         // clearTimeout( t_timeout );
         // t_timeout = load_mess( settings, 0 );


            $.ajax(settings).done(function (response) {
                //jQuery('.live-assistant__mess').html('');
               console.log(response);

                var ob_mess = response.data.messages;


                for( var i in ob_mess ) {

                    var load_mess_html2 = load_mess_html( ob_mess[i] ); 
                      
                    jQuery('.live-assistant__mess').prepend( load_mess_html2   );

                    current_offset = ob_mess[i].id.messageId;        

                }

                // clearTimeout( t_timeout );

                //  t_timeout = timeout( settings, 3000  );

                if ( !check_sync ){
                    check_sync = true;

                   // sync_chat();


                    clearTimeout( t_timeout );

                    t_timeout = sync_chat();


                }


            });
   
      }
        

    } // func


    function load_mess_html( mess_re = "" ){
        if ( !mess_re ) return;

        if ( jQuery('[data-mess-id="'+mess_re.id.messageId+'"]').length ) return;


        var template = jQuery('#template1').html();

        var mess_html = template;


        mess_html = mess_html.replace("{{class_position}}", mess_re.sender.id == ls_userId ? "messages__time-chat_right" : "" );

        mess_html = mess_html.replace("{{name}}", mess_re.sender.name );

        mess_html = mess_html.replace("{{mess_id}}", mess_re.id.messageId );



         // var hour = moment( mess_re.sender.lastActivityAt ).format('HH:mm');
         // var date = moment( mess_re.sender.lastActivityAt ).format('MM/DD/yyyy');
         // var date_all = moment( mess_re.sender.lastActivityAt ).format('DD MMM HH:mm');
       //  console.log( 'time: ' + mess_re.sender.lastActivityAt   );

          mess_html = mess_html.replace("{{date}}",  show_date( mess_re.msgDate ) );
        // mess_html = mess_html.replace("{{date}}", date_all  );
         mess_html =  mess_html.replace("{{message}}", mess_re.message );


        return mess_html;

    }


    if ( ls_token && ls_userId && ls_chatId ) {
        live_load_chat();
    } else {

        jQuery('.live-assistant__wrap_notice_mess').addClass('p-hidden');
        jQuery('.live-assistant__wrap_form').removeClass('p-hidden');  

    }






// =====================================================================================================
// sync chat
// =====================================================================================================
 function sync_chat( time = 10000 ){

    setTimeout(function(){

        if ( ls_token && ls_userId && ls_chatId ) {

            var settings = {
                
                type: "post",
                  dataType: "json",
                  url: link_ajax,
                  data: {
                    action: 'sync_mess',
                  //    abc:'test',
                    token: ls_token,
                    userId: ls_userId,
                    chatId: ls_chatId,
                    timeout:10000,
                   
                  },

            };

         // clearTimeout( t_timeout );
         // t_timeout = load_mess( settings, 0 );

            $.ajax(settings).done(function (response) {
              
              // console.log(response);
                var ob_mess = response.data.messages;


                for( var i in ob_mess ) {

                    // console.log( 'ob_mess[i]' );
                    // console.log( ob_mess[i] );

                    var load_mess_html2 = load_mess_html( ob_mess[i] ); 
                   
                    
                    jQuery('.live-assistant__mess'). append( load_mess_html2   );

            
                    //current_offset = ob_mess[i].id.messageId;
                        //console.log('current_offset: ' + current_offset );

                }

                 clearTimeout( t_timeout );

                t_timeout = sync_chat();

              
            });

           
      }

   }, time );

    
} // func




// =====================================================================================================
// button send chat
// =====================================================================================================

jQuery(document).on('click',  '.live-assistant__btn-send-chat' , function(e) {

        e.preventDefault();

        if ( jQuery('.live-assistant__input-chat').val() == "" ){
             jQuery('.live-assistant__input-chat').focus();

             return;
        }

        
        var $this = $(this);


        jQuery.ajax({
            
      type: "post",
      dataType: "json",
      url: link_ajax,
      data: {
            
            action: 'send_mess',
              //    abc:'test',
            token: ls_token,
            userId: ls_userId,
            chatId: ls_chatId,
            mess: $('.live-assistant__input-chat').val(),

      },
      context: this,
      beforeSend: function() {

         // jQuery('.live-assistant__notice_response').removeClass('p-hidden');
        
        //  p_ajax_search_show_loading( form.find('.js-ajax-search-submit') );

             // form
          //    jQuery('.live-assistant__wrap_notice_mess').addClass('p-hidden');
             // jQuery('.live-assistant__wrap_form').removeClass('p-hidden');


             // // button
             $this.attr('disabled',true);
             $this.find('i').show();
            
            //  // remove
          //    jQuery('.live-assistant__form_status').html('');

           jQuery('.live-assistant__input-chat').val('');
      },

      success: function(response) {

         $this.attr('disabled', false );
          $this.find('i').hide();

       //  func_Current = live_load_chat( 1 );

        // clearTimeout( t_timeout );

        // t_timeout = sync_chat();


          
    
    
      },

      error: function(jqXHR, textStatus, errorThrown) {
         // location.href = location.href;
          //p_user_loadding_after();

            $this.find('[type="submit"]').attr('disabled',false);
            $this.find('[type="submit"]').find('i').hide();


      }


    });





});




// =========================================================================================================
//  clear session
// =========================================================================================================
jQuery('.live-click-logout,.live-click-logout-2').on('click',function(e){
    e.preventDefault();
    location.href = location.href + '?remove';


})



// =========================================================================================================
// load offset
// =========================================================================================================

jQuery('.live-assistant__mess').on('scroll',function() {

    if ( jQuery(this).scrollTop() > 100 ) return;

  //  console.log( $(this).scrollTop() );


    if ( current_offset > 1  ) {
        
        live_load_chat('', current_offset );

    }
      
      
});





});

