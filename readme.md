Live Assistant application with Django framework

1. For first run:
# install dependencies:
pip install -r requirements.txt 
# migrate the app to execute on Django:
python manage.py migrate
# Train the model
To train the model, insert conversation to intents.json and run the 'train.py' before runserver


2. Start server with web interface:

run 'python manage.py runserver' at the root folder to start application
go to http://127.0.0.1:8000/live1/conversation/ 

