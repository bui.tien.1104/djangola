from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('live1/', include('live1.urls')),
    path('admin/', admin.site.urls),
]