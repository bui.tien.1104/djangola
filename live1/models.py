from django.db import models

class Conversation(models.Model):
	conversation_text = models.CharField(max_length=200)
	def __str__(self):
		return self.conversation_text

class Chat(models.Model):
	conversation = models.ForeignKey(Conversation, on_delete=models.CASCADE)
	subject = models.CharField(max_length=200)
	ticket = models.IntegerField(default=0)
	userid = models.IntegerField(default=0)
	def __str__(self):
		return self.subject


class Message(models.Model):
	message = models.CharField(max_length=200)
	messageDate = models.DateTimeField('MessageDate')
	chat = models.ForeignKey(Chat, on_delete=models.CASCADE)

	def __str__(self):
		return self.message
