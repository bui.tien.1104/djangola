
from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from . import chat, nltk_utils, openaiEmbed
import json, random


def index(request):
    return render(request, 'live1/index.html')


def conversation(request):
    return render(request, 'live1/base.html')


@csrf_exempt
def predict(request):
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        body_data = json.loads(body_unicode)
        message = body_data['message']
        tag, response = chat.get_response(message)
        # prob = prob * 100
        # response = chat.get_response(message)
        # tag = chatGPT.find_subject(message)
        return JsonResponse({'tag': tag, 'response': response})
    # return JsonResponse({'answer': str(response)})
    else:
        return JsonResponse({'Message is not found in POST data'})


