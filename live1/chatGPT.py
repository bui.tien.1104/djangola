import openai
import spacy
from nltk.corpus import stopwords

openai.api_key = "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"

# spacy.cli.download("en_core_web_sm")
stopword = stopwords.words('english')


def find_subject(sentence):
	nlp = spacy.load("en_core_web_sm")
	doc = nlp(sentence)
	subj = []
	for token in doc:
		print(token.text, token.pos_, token.dep_)
		if token.pos_ in ["ADJ"] and token.dep_ in ["amod", "acomp"] not in stopword:
			subj.append(token)
		if token.pos_ in ["NOUN", "PROPN"] and token.dep_ in ["nsubj", "pobj", "dobj", "compound"]:
			subj.append(token)

	return subj


def get_response(msg):
	openai.api_key = "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"
	response = openai.Completion.create(
		model="text-davinci-003",
		prompt=msg,
		temperature=0.8,
		max_tokens=500
	)
	return [response.choices[0].text.strip()]

# Call the function to get the response
# openai.api_base = "https://api.openai.com/v1"
#
# response = openai.ChatCompletion.create(
#     model="gpt-3.5-turbo",
#     messages=[
#         {"role": "system", "content": "You are a service provider. Please assist the user with their inquiries."},
#         {"role": "user", "content": "Who won the world series in 2020?"},
#         {"role": "assistant", "content": "The Los Angeles Dodgers won the World Series in 2020."},
#         {"role": "user", "content": "Where was it played?"}
#     ]
# )
#
# print(response['choices'][0]['message']['content'])

# openai api completions.create -m davinci:ft-personal-2023-06-05-07-08-36 -p
