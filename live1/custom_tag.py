from django import template
from django.templatetags.static import PrefixNode

register = template.Library()

@register.simple_tag(takes_context=True)
def get_current_script(context):
    request = context['request']
    scheme = 'https' if request.is_secure() else 'http'
    domain = request.get_host()
    path = request.path
    return f"{scheme}://{domain}{path}"
