
import openai  # for calling the OpenAI API

# spacy.cli.download("en_core_web_sm")
openai.api_key = "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"
# models
EMBEDDING_MODEL = "text-embedding-ada-002"
GPT_MODEL = "gpt-3.5-turbo"


def get_response(text):

	query = f""" Access the below link and all its paths to answer the subsequent question, if not found, tell customer to retype the correct keyword.
	
	link:  
	https://www.iptp.net/en_US
	
	Question: {text}?
	"""

	response_set = openai.ChatCompletion.create(
		messages=[
			{'role': 'system', 'content': ' You are an customer-relation employee of IPTP networks answering questions for customers '},
			{'role': 'user', 'content': query},
		],
		model=GPT_MODEL,
		temperature=1,
		n=2

	)
	response=[]
	for choice in response_set.choices:
		response.append(choice["message"]["content"])
	return response

# print(responses['choices'][0]['message']['content'])
